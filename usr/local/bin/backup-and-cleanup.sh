#!/bin/bash 
#
# Backup database and detailed radacct records to remote destination (in fstab) 
#  while removing the radacct files from local storage
#
# The /bakup/database and /backup/radacct directories must exist before running this
#
# Ali Khalil
# 2014-08-11
#

# Define directory paths
DIR_BACKUP=/backup
DIR_DATA=/data
DIRS_RADACCT=/data/var/log/freeradius/radacct/*

# Database Information
DB_NAME=radius
DB_USER=root
DB_PASS=3x1651a

function doit() {
	echo "[*] Starting backup process..."
	echo "  [*] Configuration Parameters"
	echo "    [=] Backup Destination: $DIR_BACKUP"
	echo "    [=] Data Source: $DIR_DATA"
	echo "    [=] RADIUS Accounting Source: $DIRS_RADACCT"
	echo "    [=] Database Table: $DB_NAME"

	echo "  [*] Backup destination mount."
	# Ensure backup destination is mounted
	if ! mountpoint -q "$DIR_BACKUP" ; then
		echo "  [*] Attempting to mount $DIR_BACKUP"
		mount $DIR_BACKUP && echo "    [+} Backup target mounted successfully." || failed_error
	else
		echo "    [=] Backup destination $DIR_BACKUP already mounted."
	fi 

	BACKUP_MYSQL_DESTINATION=$DIR_BACKUP/database/$DB_NAME-$(date +%Y%m%d%H%M%S).sql
	echo "  [*] Database backing up to $BACKUP_MYSQL_DESTINATION"
	mysqldump -u $DB_USER -p$DB_PASS $DB_NAME > $BACKUP_MYSQL_DESTINATION \
		&& echo "    [+] Backed up FreeRADIUS database successfully."  \
		|| failed_error

	# Deal with the files
	echo "  [*] Processing files"

	# Initialize total file count
	COUNT_FILES=0

	# Multiple BRAS aggregators possible. Handle all of them.
	for DIR_RADACCT in $DIRS_RADACCT; do

		# Extract Host IP from directory name
		HOST_RADACCT=$(basename $DIR_RADACCT)

		# Generate backup destination for Host
		DIR_HOST_RADACCT=$DIR_BACKUP/radacct/$HOST_RADACCT

		# Create Host backup destination directory if doesn't already exist
		if [ ! -d $DIR_HOST_RADACCT ]; then 
			echo "      [*] Creating radacct backup directory for host $HOST_RADACCT at $DIR_HOST_RADACCT"
			mkdir $DIR_HOST_RADACCT || failed_error
		else
			echo "      [=] Detailed accounting records backup directory for $HOST_RADACCT exists."
		fi

		# Find all files that don't match TODAY's file name
		FILES=`find $DIR_RADACCT -type f ! -name "detail-$(date +%Y%m%d)"` 

		# Count number of files in directory
		NUMFILES=`echo $FILES | wc -w`

		# Move matched files if they exist
		if [ $NUMFILES != "0" ]; then
			for FILE in $FILES; do
				echo "        [+] Moving file: $FILE" 
				mv $FILE $DIR_HOST_RADACCT || failed_error
			done

			# Add this directories number of files to total count
			COUNT_FILES=`expr $COUNT_FILES + $NUMFILES`

		else
			echo "        [+] No files match processing criteria"
		fi

	done

	echo "      [*] Processed files count: $COUNT_FILES"

	echo "  [*] Commit and push to Git repositories."
	git_actions | logger -i -t git &
	echo "    [=] Check syslog messages for PID $!"

	echo "[*] Backup finished successfully."
}

function git_actions() {
	# Commit and Push git 
	cd $DIR_DATA
	git add $DIR_DATA/etc  $DIR_DATA/usr $DIR_DATA/opt
	git commit -m "`date`" 
	git remote | xargs -l git push
}

function failed_error() {
	echo "[!] Backup process failed to complete."
	exit 1
}

doit | logger -i -t backup

